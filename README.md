# Pokédex Application - Interview_Test

![Pokédex App](https://i.ibb.co/87TnKBK/pokemon-API.jpg)

## User Guide
- **Download** Pokédex Application && clone the project using `git clone`.
- **Run** in terminal: `npm install`.
- **Run** in terminal: `git checkout develop`.
- **Run** in terminal: `npm start`.

## Introduction
The Pokédex is an application that you allow to view 
some Pokemon's information like weight, height, abilities and much more.
This App is using requests from PokeAPI: **https://pokeapi.co**

## Technologies
- Pokemon API.
- Boostrap.
- ReactJS.
- Html.
- Css.
- Js.
