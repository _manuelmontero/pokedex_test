import { useState, useEffect } from 'react';
import { getPokemonById, getPokemons } from './api/pokemon';
import './App.css';

const App = () => {
  const [pokemons, setPokemons] = useState([]);
  const [currentPokemonId, setCurrentPokemonId] = useState(1);
  const [details, setDetails] = useState({
    Name: "",
    Habilidad: [],
    PokemonType: [],
    Weight: 0,
  });

  useEffect(() => {
    getPokemons().then((data) => setPokemons(data.results));
  }, []);

  useEffect( async() => {
    const response = await getPokemonById(currentPokemonId);
    const {weight, height, species, abilities, types, name} = response;
    console.log("La respuesta es: ", response);
    console.log("Los tipos EN STRING #1: ", JSON.stringify(types[0].type.name));

    setDetails({
      Name: name,
      Abilities: abilities,
      Types: [types[0].type.name],
      // , types[1].type.name],
      Weight: weight,
      Height: height,
      Species: species.name,
    })
  }, [currentPokemonId]);

  const showDetails = (pokemon) => {
    const idPokemon = Number(pokemon.url.split("/").slice(-2)[0]);
    setCurrentPokemonId(idPokemon);
  }

  return (
    <div className="App">
      {pokemons.map(pokemon => 
        <div key={pokemon.name} onClick={() => showDetails(pokemon)}>
          {pokemon.name}
        </div>) }

      {details && (
        <aside>
          <h1>Information</h1>
          <h4>Weight: {details.Weight} lbs.</h4>
          <h4>Height: {details.Height}</h4>
          <h4>Species: {details.Species}</h4>
          <h4>Egg Groups: Dato quemado</h4>
          {/* <h4>Abilities: {details.Abilities}</h4> */}
          <h4>Type: {details.Types}</h4>
          
        </aside>
      )}
      
    </div>
  );
}

//module.exports = App;
export default App;