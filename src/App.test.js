import { render, screen } from '@testing-library/react';
import App from './App';
import data from "./data.json";

describe("Pokemon APP", () => {
  beforeAll(() => jest.spyOn(window, "fetch"));

  it("Should show a list of Pokemons from an PokeAPI", async() => {
    window.fetch.mockResolvedValueOnce({
      ok: true,
      json: async () => data,
    });

    render(<App />);
    expect(window.fetch).toHaveBeenCalledTimes(1);
    expect(window.fetch).toHaveBeenCalledWith("https://pokeapi.co/api/v2/pokemon/?limit=25");

    for(let pokemon of data.results){
      expect(await screen.findByText(pokemon.name)).toBeInTheDocument();
    }
  });
});
